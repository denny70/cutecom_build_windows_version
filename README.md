## Purpose
There are "Linux version of Cutecom", but it seem there is no "Mac version of Cutecom" or I do not find it on the internet. so I take some time to build it.
the Cutecom source code is from https://gitlab.com/cutecom/cutecom/.
the version is :
commit 70d0c497acf8f298374052b2956bcf142ed5f6ca (HEAD -> master, origin/master, origin/HEAD)
Author: Meinhard R cyc1ingsir@gmail.com
Date:   Thu Jun 11 18:33:10 2020 +0000
Fix-build-with-Qt-5.15 (hopefully)

## What do I do for this version

1. To use qmake
2. To use Cmake GUI tool of QT
3. To add "#include " in the h file "ctrlcharacterspopup.h", because do make will fail so add this
4. mingw32-make (on windowd 7)

OS: Windows 7 , QT version 5.12.2, Creator 4.14.2,  tested on Win7 and Win10 CuteCom work 
